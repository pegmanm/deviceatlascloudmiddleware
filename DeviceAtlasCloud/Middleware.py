import re
from django.conf import settings
from DeviceAtlasCloud import Client


class DeviceAtlasCloudMiddleware(object):
	"""
		Get the properties of a device from its UA.

		DeviceAtlasCloudMiddleware intercepts UAs from within Django
		and forwards them to the DeviceAtlasCloud API.	It will then
		insert the device properties into the request context.
	"""
	def process_request(self, request):
		domain = request.get_host()
		path = request.get_full_path()
		referer = request.META.get('HTTP_REFERER', None)

		if not self._is_ignorable_url(path) and not self._is_internal_request(domain, referer):
			ua = request.META.get('HTTP_USER_AGENT', '<none>')
			accept_h = request.META.get('HTTP_ACCEPT_ENCODING', '<none.')
			header = {'user_agent': '%s' % ua, 'accept': '%s' % accept_h}

			try:
				# instantiate DeviceAtlas and perform look up
				da = Client.Client()
				da_data = ()
				da_data = da.getDeviceData(header)
				request.da = da_data
			except:
				pass

	"""
		Returns true if the referring URL is the same domain as the current request.
	"""
	def _is_internal_request(self, domain, referer):
		return referer is not None and re.match("^https?://%s/" % re.escape(domain), referer)

	"""
		Returns True if a given URL *shouldn't* be passed to DeviceAtlasCloud
	"""
	def _is_ignorable_url(self, uri):
		for start in settings.DA_IGNORABLE_URL_STARTS:
			if uri.startswith(start):
				return True
		for end in settings.DA_IGNORABLE_URL_ENDS:
			if uri.endswith(end):
				return True
		return False
