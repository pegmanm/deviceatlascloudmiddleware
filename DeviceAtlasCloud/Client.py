#!/usr/bin/env python
# coding: utf-8

'''
Welcome to DeviceAtlas Cloud! All you need to get going is to set your DeviceAtlas
licence key below and import this module into your code.

Device data can then be retrieved as follows:

    import DeviceAtlasCloud.Client
    da = DeviceAtlasCloud.Client.Client()
    headers = {'user_agent': 'iPhone'}
    data = da.getDeviceData(headers)

The returned data will contain the following:

    data['properties']  - contains the device properties
    data['_error']  - contains any errors that occurred when fetching the properties
    data['_source'] -  states where the data came from, one of: 'cookie', 'cache', 'cloud' or 'none'.
    data['_useragent'] - contains the useragent that was used to query for data

@copyright Copyright © 2011 dotMobi. All rights reserved.
'''

__author__ = 'Ronan Cremin (tech@mtld.mobi)'
__copyright__ = 'Copyright © 2011 dotMobi. All rights reserved.'

import sys, urllib, urllib2, httplib, hashlib, os, json, tempfile, time, pprint


class Client:

    def __init__(self):
        ######################################################
        # BASIC SETUP
        # This is all you need to edit to get going
        from django.conf import settings
        self.LICENSE_KEY = settings.DA_LICENSE_KEY

        # ADVANCED SETUP
        # Edit these if you want to tweak behaviour

        self.USE_FILE_CACHE = True
        self.CACHE_ITEM_EXPIRY_SEC = 2592000;           # 2592000 = 30 days in seconds
        self.CACHE_NAME = 'deviceatlas_cache';
        self.CLOUD_HOST = 'api.deviceatlascloud.com'
        self.CLOUD_PORT = '80'
        self.CLOUD_PATH = '/v1/detect/properties?licencekey=%s&useragent=%s'
        self.TEST_USERAGENT = 'iphone'
        self.USE_SYSTEM_TEMP_DIR = True                 # leave as True to use system-defined default temp directory
        self.CUSTOM_CACHE_DIR = '/path/to/your/cache/'  # used only if USE_SYSTEM_TEMP_DIR is False

        # END OF SETUP, no need to edit below here!
        ######################################################

        # CONSTANTS
        self.USERAGENT = '_useragent';
        self.SOURCE = '_source';
        self.ERROR = '_error';
        self.PROPERTIES = 'properties';
        self.SOURCE_COOKIE = 'cookie';
        self.SOURCE_FILE_CACHE = 'cache';
        self.SOURCE_CLOUD = 'cloud';
        self.SOURCE_NONE = 'none';
        self.DA_HEADER_PREFIX = 'X-DA-'
        self.API_VERSION = 'python/1.0'
        # END CONSTANTS


    def getDeviceData(self, headers={}, test_mode = False):
        '''Returns device data for given user agent, headers is dict of form:
          {'user_agent': 'Nokia6300', 'accept': 'text/html', 'x_profile': '..',
          'x_wap_profile': '..', 'accept_language': '..'}

        Once data has been returned from DeviceAtlas cloud it can be cached
        locally to speed up subsequent results.
        '''

        results = {}
        results[self.SOURCE] = self.SOURCE_NONE

        if test_mode:
            headers['user_agent'] = self.TEST_USERAGENT

        user_agent = headers['user_agent']

        try:
            device_data = ''

            # try file cache first
            if self.USE_FILE_CACHE:
                device_data = self.cacheGet(user_agent)
                results[self.SOURCE] = self.SOURCE_FILE_CACHE

            # fall back to fetching data from cloud
            if device_data == '':
                device_data = self.cloudGet(headers)
                results[self.SOURCE] = self.SOURCE_CLOUD

            # decode json
            results[self.PROPERTIES] = self.decodeData(device_data)

            # set caches for future queries
            self.setCaches(user_agent, device_data, results[self.SOURCE])
        except Exception, err:
            results[self.ERROR] = str(err)
        return results


    def cloudGet(self, headers):
        '''Fetches device data from DeviceAtlas Cloud service. Passed data
        must be a dictionary of HTTP headers'''

        path = self.CLOUD_PATH % (self.LICENSE_KEY, urllib.quote(headers['user_agent']))
        del headers['user_agent']

        # build request
        req = urllib2.Request('http://' + self.CLOUD_HOST + ':' + self.CLOUD_PORT + path)

        # add any additional headers supplied and API version as X-DA-* headers
        headers['Version'] = self.API_VERSION
        for header, value in headers.items():
            req.add_header(self.DA_HEADER_PREFIX + header, value)

        try:
            resp = urllib2.urlopen(req)
        except urllib2.HTTPError, e:
            raise Exception('Error fetching DeviceAtlas data from Cloud. Code: ' + str(e.code) + ' ' + e.read().strip() )

        device_data = resp.read()
        return device_data


    def setCaches(self, user_agent, device_data, source):
        '''Stores DeviceAtlas Cloud device data in file cache for later use'''

        if self.USE_FILE_CACHE and source == self.SOURCE_CLOUD:
            self.cachePut(user_agent, device_data)
        return


    def cacheGet(self, user_agent):
        '''Try and find devices data from the file cache'''

        device_data = ''

        path = self.getCachePath(hashlib.md5(user_agent).hexdigest())

        if os.path.exists(path):
            mtime = os.path.getmtime(path)
            if mtime + self.CACHE_ITEM_EXPIRY_SEC > time.time():
                device_data = open(path).read()

        return device_data


    def cachePut(self, user_agent, device_data):
        '''Put the device data in the file cache'''

        res = True

        path = self.getCachePath(hashlib.md5(user_agent).hexdigest())
        os.makedirs(os.path.dirname(path), mode=0755)
        try:
            fh = open(path, 'w')
            fh.write(device_data)
        except:
            raise Exception('Cannot write cache file data at "%s"' % path)

        return res


    def getCachePath(self, md5):
        '''Creates a cache path for this item by taking the md5 hash
        and using the first 4 characters to create a directory structure.
        This is done to prevent too many files existing in any one directory
        as this can lead to slowdowns.'''

        first_dir = md5[0:2]
        second_dir = md5[2:4]
        file_name = md5[4:len(md5)]

        base_path = ''

        if self.USE_SYSTEM_TEMP_DIR:
            base_path = tempfile.gettempdir()
        else:
            base_path = self.CUSTOM_CACHE_DIR

        base_path += os.sep + self.CACHE_NAME + os.sep
        return base_path + first_dir + os.sep + second_dir + os.sep + file_name


    def decodeData(self, device_data):
        '''Decodes the JSON data and extracts the properties'''

        props = ''
        if not device_data == {}:
            decoded = json.loads(device_data)
            if decoded.has_key(self.PROPERTIES):
                props = decoded[self.PROPERTIES]
            else:
                raise Exception('Cannot get device properties from "%s"' % device_data)

        return props


def test():
    '''Basic tests of cloud lookup'''

    da = Client()
    pp = pprint.PrettyPrinter(indent = 4)

    # basic test device, no headers supplied
    data = da.getDeviceData(test_mode=True)
    print '\nTest mode results:'
    pp.pprint(data)

    # user-agent and accept headers sent
    headers = {
            'user_agent': 'SEC-SGHX210/1.0 UP.Link/6.3.1.13.0',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
            }
    data = da.getDeviceData(headers)
    print '\nExample including accept header:'
    pp.pprint(data)

    # larger set of headers
    headers = {
            'user_agent': 'Mozilla/5.0 (Linux; U; Android 2.2; zh-cn; HTC_Desire_A8181 Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1',
            'accept': 'application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5',
            'accept_language': 'zh-CN, en-US',
            'x_wap_profile': 'http://www.htcmms.com.tw/Android/Common/Bravo/HTC_Desire_A8181.xml'
            }
    data = da.getDeviceData(headers)
    print '\nExample including multiple headers:'
    pp.pprint(data)


if __name__ == '__main__':
    test()

